******************
CodeWars Solutions
******************

`My profile on CodeWars. <https://www.codewars.com/users/Raythor>`_

Here I present my solutions for CodeWars Katas in different programming
languages.

Cheat Sheets
************

Here I have a several cheat sheets to all languages I trained, feel free
to see them.

- `Git <https://gitlab.com/cuk_monsta/codewars-solutions/blob/master/cheat-sheets/git/git_cheatsheet.rst>`_
- `Python <https://gitlab.com/cuk_monsta/codewars-solutions/blob/master/cheat-sheets/python/python_cheatsheet.rst>`_
- `Shell (Bash) <https://gitlab.com/cuk_monsta/codewars-solutions/blob/master/cheat-sheets/bash/bash_cheatsheet.rst>`_
- `SQL (PostgreSQL) <https://gitlab.com/cuk_monsta/codewars-solutions/blob/master/cheat-sheets/sql/sql_cheatsheet.rst>`_

My Statistics
*************

My Progress
-----------

- Rank: 3 kyu
- Honor: 903
- Total Completed Kata: 97

Languages and Rank Breakdown
----------------------------

- Total Languages Trained: 5
    - Python (4 kyu)
    - SQL (5 kyu)
    - Shell (7 kyu)
    - C++ (8 kyu)
    - Scala (8 kyu)
