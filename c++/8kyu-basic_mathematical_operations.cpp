/*
Your task is to create a function that does four basic mathematical operations.

The function should take three arguments - operation(string/char), value1(number), value2(number).

The function should return result of numbers after applying the chosen operation.

Examples:

basicOp('+', 4, 7)         // Output: 11
basicOp('-', 15, 18)       // Output: -3
basicOp('*', 5, 5)         // Output: 25
basicOp('/', 49, 7)        // Output: 7
/**/

// My Answer
int basicOp(char op, int val1, int val2) {
  if(op == '+'){
    return val1 + val2;
  }  else if(op == '-'){
       return val1 - val2;
  }  else if(op == '*'){
       return val1 * val2;
  }  else if(op == '/'){
       return val1 / val2;
  }
}

// Best Answer
int basicOp(char op, int val1, int val2) {
  switch (op) {
    case '+': return val1 + val2;
              break;
    case '-': return val1 - val2;
              break;
    case '*': return val1 * val2;
              break;
    case '/': return val1 / val2;
              break;
    default: return -99999;
  }
}
