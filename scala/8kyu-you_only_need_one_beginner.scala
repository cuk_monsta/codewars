/*
You will be given an array "a" and a value "x". All you need to do is check whether the provided
array contains the value.

Array can contain numbers or strings. "X" can be either.

Return true if the array contains the value, false if not.
*/

# My Answer
object Solution {
  def check(seq:List[Any], elem: Any): Boolean = {
    if(seq.contains(elem))
      return true
    else
      return false
  }
}

# Best Answer
object Solution {
  def check(seq:List[Any], elem: Any) = seq.contains(elem)
}
