-- For this challenge you need to create a simple SELECT statement that will
-- return all columns from the products table, and join to the companies table
-- so that you can return the company name.

-- products table schema
--  id
--  name
--  isbn
--  company_id
--  price

-- companies table schema
--  id
--  name

-- My Answer
SELECT p.*, c.name AS company_name
FROM products AS p LEFT JOIN companies AS c
  ON p.company_id = c.id
GROUP BY p.name

-- Best Answer
SELECT products.*, companies.name AS company_name
FROM products JOIN companies ON company_id = companies.id
