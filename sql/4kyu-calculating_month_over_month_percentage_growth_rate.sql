-- Given a posts table that contains a created_at timestamp column write a
-- query that returns a first date of the month, a number of posts created
-- in a given month and a month-over-month growth rate.

-- The resulting set should be ordered chronologically by date.

-- NOTE:
--  Percent growth rate can be negative.
--  Percent growth rate should be rounded to one digit after the decimal point
--  and immediately followed by a percent symbol %. See the desired output
--  below for the reference.

-- desired output table schema
--     date    | count | percent_growth
--  2016-02-01 |  175  |       null
--  2016-03-01 |  338  |      93.1%

-- date (DATE)
-- count (INT)
-- percent_growth (TEXT)

-- My Answer
WITH q_table AS (
  SELECT date_trunc('month', created_at)::DATE AS date, COUNT(title) AS count
  FROM posts
  GROUP BY date
  ORDER BY date
)

SELECT q_table.date, q_table.count,
       CAST(ROUND(CAST((q_table.count - LAG(q_table.count) OVER (ORDER BY q_table.date))::float /
       (LAG(q_table.count) OVER (ORDER BY q_table.date)) * 100 AS numeric), 1) AS text) || '%' AS percent_growth
FROM q_table

-- Best Answer
select date_trunc('month', created_at)::date as date
  , count(*) as count
  , round(((100.0 / (lag(count(*)) over (order by date_trunc('month', created_at)::date asc))) * count(*)) -100, 1) || '%' as percent_growth
from posts
group by date
order by date asc
