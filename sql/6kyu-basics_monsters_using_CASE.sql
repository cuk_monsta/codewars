-- You have access to two tables named top_half and bottom_half, as follow:

-- top_half schema
-- id
-- heads
-- arms

-- bottom_half schema
-- id
-- legs
-- tails

-- output schema
-- id
-- heads
-- legs
-- arms
-- tails
-- species

-- The IDs on the tables match to make a full monster. For heads, arms, legs
-- and tails you need to draw in the data from each table.

-- For the species, if the monster has more heads than armsn, more tails than
-- legs, or both, it is a 'BEAST', else it is a 'WEIRDO'. This needs to
-- be captured in the species column.

-- All rows should be returned is 10, and this exercise require the use
-- of CASE, order by species.

-- My Answer
SELECT *,
  CASE
    WHEN heads > arms OR tails > legs THEN 'BEAST'
    ELSE 'WEIRDO'
    END as species
FROM top_half
INNER JOIN bottom_half
  ON top_half.id = bottom_half.id
ORDER BY species

-- Best Answer
SELECT
  th.id,
  th.heads,
  bh.legs,
  th.arms,
  bh.tails,
  CASE WHEN th.heads > th.arms or bh.tails > bh.legs
    THEN 'BEAST'
    ELSE 'WEIRDO'
  END as species

FROM top_half th

INNER JOIN bottom_half bh
  on bh.id = th.id

ORDER BY species
