-- Given a demographics table in the following format:

-- demographics table schema
--  id
--  name
--  birthday
--  race

-- Return a single column named calculation where the value is the bit length
-- of name, added to the number of characters in race.

-- My Answer
SELECT BIT_LENGTH(name) + LENGTH(race) AS calculation
FROM demographics

-- Best Answer
select bit_length(name)+length(race) calculation from demographics
