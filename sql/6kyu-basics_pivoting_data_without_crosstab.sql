-- This kata is inspired by SQL Basics: Simple PIVOTING data by matt c.

-- You need to build a pivot table WITHOUT using CROSSTAB function.
-- Having two tables products and details, you need to select a pivot table
-- of products with counts of details occurrences (possible details values are
-- ['good', 'ok', 'bad']).

-- Results should be ordered by product's name.

-- Model schema for the kata is:

-- products table schema
--  id (key)
--  name

-- details table schema
--  id
--  product_id (key)
--  detail

-- Your query should return:

-- output table schema
--  name
--  good
--  ok
--  bad

-- Compare your table to the expected table to view the expected results.

-- My Answer
SELECT name, MAX(good) AS good, MAX(ok) AS ok, MAX(bad) AS bad
FROM (SELECT products.name,
  (CASE
    WHEN details.detail = 'good' THEN COUNT(details.detail)
  END) AS good,
 (CASE
    WHEN details.detail = 'ok' THEN COUNT(details.detail)
  END) AS ok,
 (CASE
    WHEN details.detail = 'bad' THEN COUNT(details.detail)
  END) AS bad
  FROM products INNER JOIN details
    ON products.id = details.product_id
  GROUP BY products.name, details.detail
  ORDER BY products.name
  ) AS result
GROUP BY result.name

-- Best Answer
SELECT p.name,
  count(case when d.detail = 'good' then 1 end) as good,
  count(case when d.detail = 'ok'  then 1 end) as ok,
  count(case when d.detail = 'bad' then 1 end) as bad
FROM products p
INNER JOIN details d ON p.id = d.product_id
GROUP BY p.name
ORDER BY p.name

