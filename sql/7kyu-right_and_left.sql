-- You are given a table named repositories, format as below:

-- repositories table schema
--  project
--  commits
--  contributors
--  address

-- The table shows project names of major cryptocurrencies, their numbers of
-- commits and contributors and also a random donation address (not linked
-- in any way :)).

-- For each row: Return first x characters of the project name where
-- x = commits. Return last y characters of each address where
-- y = contributors.

-- output table schema
--  project
--  address

-- Case should be maintained.

-- My Answer
SELECT LEFT(project, commits) AS project, RIGHT(address, contributors) AS address
FROM repositories

-- Best Answer
select left(project, commits) as project, right(address, contributors) as address
from repositories
