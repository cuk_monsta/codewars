-- Given a demographics table in the following format:

-- demographics table schema
--  id
--  name
--  birthday
--  race

-- You need to return the same table where all text fields (name and race)
-- are changed to the bit length of the string.

-- My Answer
SELECT id, BIT_LENGTH(name) AS name, birthday, BIT_LENGTH(race) AS race
FROM demographics

-- Best Answer
SELECT id, BIT_LENGTH(name) as name, birthday, BIT_LENGTH(race) as race
FROM demographics;
