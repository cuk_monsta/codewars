-- Given the table below:

-- names table schema
--  id
--  prefix
--  first
--  last
--  suffix

-- Your task is to use a select statement to return a single column table
-- containing the full title of the person (concatenate all columns together
-- expect id), as follow:

-- output table schema
--  title

-- NOTE: Don't forget to add spaces.

-- My Answer
SELECT CONCAT(prefix,' ', first,' ', last,' ', suffix) as title
FROM names

-- Best Answer
SELECT concat_ws(' ', prefix,first,last,suffix) AS title FROM names;
