Commands
--------

Select by position in a string

.. code-block:: bash

   echo ${variable:5} # the number is the position

Select by position and length in a string

.. code-block:: bash

   echo ${variable:5:2} # the first number is the position and the second is the length

Show the number of rows in a given file.

.. code-block:: bash

   wc -l <filename>

A cow give you the message you want.

.. code-block:: bash

   echo 'string you want' | cowsay

Show the structure of a directory

.. code-block:: bash

   tree <directory> | head

To revert a chain of characters use:

.. code-block:: bash

   rev <<< 'string'

How to generate a sequence of numbers

.. code-block:: bash

   seq <number>

Show the header of the file

.. code-block:: bash

   head <path/to/file>
   head -n <number> <path/to/file>

Show the number of lines, characters and words

.. code-block:: bash

   echo 'Hello world' | wc

Define a function to calculate the factorial of a number,

.. code-block:: bash

   fac() { (echo 1; seq $1) | paste -s -d\* | bc; }

Define a alias, it is like macros, it doesn't allow parameters.

.. code-block:: bash

   alias l='ls -1 --group-directories-first'
   alias moer=more
# Show a list of the directories and files in our position
# it show the list with one column and order the directories first

Show what kind of command it is

.. code-block:: bash

   type -a <command>

Filter lines in a file with grep command

.. code-block:: bash

   cat <filename> | grep <filter>

Examples:

Filter a number sequence, we only want the numbers that contains 3

.. code-block:: bash

   seq 30 | grep 3

How many numbers between 1 and 100 contain a 3

.. code-bloc:: bash

   seq 100 | grep 3 | wc -l

Redirecting Input and Output, we save the output in a file

.. code-block:: bash

   seq 10 > <filename>  # Overwrite the file if it exists

   seq 10 >> <filename>  # Add the content at the end of the file after the
                         # original content

Wraps long lines to 80 chracters

.. code-block:: bash

   fold

Trims lines that are longer than 80 characters.

.. code-block:: bash

   cut -c1-80

To do a secure copy of a file onto the EC2 instance in an interactive mode

.. code-block:: bash

   scp -i mykey.pem ~/Desktop/logs.csv \
   ubuntu@ec2-184-73...etc

Descompressing Files with tar, unzip or unrar

.. code-block:: bash

   tar -xzvf <filename>

# the option we used are x-extract z-use gzip as the decompression
algorithm v-verbose and f-to use the <filename>

# We can use other option to descompressing files as unpack

.. code-block:: bash

   unpack <filename>

Converting Microsoft Excel Spreadsheets

.. code-block:: bash

   in2csv <file.xlsx> <file.csv>

To read a xlsx file we can use

.. code-block:: bash

   in2csv <file.xlsx> | head | cut -c1-80

# The tool in2csv extract only the first worksheet, to extract a different
# worksheet we need to pass the name of worksheet to the --sheet option.

To read a CSV file, we can do (csvlook)

.. code-block:: bash

   in2csv <file.xlsx> | head | csvcut -c Column1,Column2,Column3 | csvlook

# To use those commands we need to install csvkit in our system, also
# it contains a sql2csv too.

Querying relational databases

.. code-block:: bash

   sql2csv --db <database> --query <query-we-want>
   sql2csv --db 'sqlite:///data/iris.db' --query 'SELECT * FROM iris'\
   'WHERE sepal_length > 7.5'

Download data from Internet HTTP/HTTPS protocol

.. code-block:: bash

   curl -s <URL> # we can use curl -s <URL> | head -n 10 para mostrar 10 lines
   # the option -s is silent, so that the progress meter is disabled if we
   # want to uso another command with curl

Download data from Internet FTP protocol

.. code-block:: bash

   curl -u username:password ftp://host/File

When you access a shortened URL as hhtp://bit.ly/, the browser automatically
redirects you to the correct location if you specify -L or --location option.

.. code-block:: bash

   curl -L j.mp/locatbbar

If you want to show the details of the URL

.. code-block:: bash

   curl -I or --head <URL>

Some APIs require you to log in using the OAuth protocol.
There is a convenient command-line tool called curlicue that assits in
performing the so-called Oauth dance. Once this has been set up, curlicue
will call curl with the correct headers. First, you set everything up for
a particular API with curlicue-setup, and then you can call that API
using curlicue. For example, to use curlicue with the Twitter API
you would run:

.. code-block:: bash

   $ curlicue-setup \
   > 'https://api.twitter.com/oauth/request_token' \
   > 'https://api.twitter.com/oauth/authorize?oauth_token=$oauth_token' \
   > 'https://api.twitter.com/oauth/access_token' \
   > credentials
   $ curlicue -f credentials \
   > 'https://api.twitter.com/1/statuses/home_timeline.xml'


Converting One-Liners into Shell Scripts
----------------------------------------

We're going to explain how to turn a one-liner into a reusable
command-line tool, imagiine that we have the following one-liner:

.. code-block:: bash

    $ curl -s http://www.gutenberg.org/cache/epub/76/pg76.txt | (1)
    > tr '[:upper:]' '[:lower:]' | (2)
    > grep -oE '\w+' | (3)
    > sort | (4)
    > uniq -c | (5)
    > sort -nr | (6)
    > head -n 10 (7)

That code returns the top ten words of the ebook version of
Adventures of Huckleberry Finn, let's see:

1 - Downloading the ebook using curl.
2 - Converting the entire text to lowercase using tr.
3 - Extracting all the words using grep and putting each word
    on a separate line.
4 - Sorting these words in alphabetical order using sort.
5 - Removing all the duplicates and counting how often each
    word appears in the list using uniq.
6 - Sorting this list of unique words by their count in descending
    order using sort.
7 - Keeping only the top 10 lines using head.

To turn this one-liner into a reusable command-line tool, we'll
walk you through the following six steps:

1 - Copy and paste the one-liner into a file.

    Create a new file, open your favorite text editor and copy
    and paste our one-liner. We name the file top-words-1.sh.

    We're using the file extension .sh to make clear that we're
    creating a shell script. However, command-line tools don't need
    to have an extension. In fact, command-line tools rarely have extensions.

* Trick: !! sustituira por el ultimo comando utilizado

2 - Add execute permissions.

    To change the access permissions of a file, we use a
    command-line tool called "chmod" which stands for change mode,
    it changes the file mode bits of a specific file. The following
    command gives the user, you, the permision to execute our script.

.. code-block:: bash

    chmod u+x top-words-2.sh

    The u+x option consists of three characters, u indicates that we want
    to change the permissions for the user who owns the file, which is you,
    because you created the file, + indicates that we want to add a permissions,
    and x, which indicates the permissions to execute.

    Let's have a look at the access permissions of both files:

.. code-block:: bash

    ls -l top-words-{1,2}.sh

    The first character indicates the file type:

    - '-' means regular file
    - 'd' means directory

    The next three characters, rwx, indicate the access permissions
    for the user who owns the file.

3 - Define a so-called shebang.
4 - Remove the fixed input part.
5 - Add a parameter.
6 - Optionally extend your PATH.

