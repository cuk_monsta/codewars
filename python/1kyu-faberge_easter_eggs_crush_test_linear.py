"""
This is the SUPER performance version of this Kata (faberge-easter-eggs-crush-test).

You task is exactly the same as that kata. But this time, you should output result % 998244353, or
otherwise the result would be too large.

Data Range

sometimes
  n <= 80000
  m <= 100000

while sometimes
  n <= 3000
  m <= 2**200

There are 150 random test. You will need more than just a naive linear algorithm for this task :D.
"""

# My Answer
MOD = 998244353
_inv = [0, 1]
for _i in range(2, 80000 + 1):
        _inv.append( (MOD - MOD // _i) * _inv[MOD % _i] % MOD )

def height(n, m):
    h, t = 0, 1
    m %= MOD

    for i in range(1, n + 1):
        t = t * (m - i + 1) * _inv[i] % MOD
        h = (h + t) % MOD
    return h % MOD

# Best Answer
MOD = 998244353

def height(n, m):
    m %= MOD
    inv = [0] * (n + 1)
    last = 1
    ans = 0
    for i in range(1, n + 1):
        inv[i] = -(MOD // i) * inv[MOD % i] % MOD if i > 1 else 1
        last = last * (m - i + 1) * inv[i] % MOD
        ans = (ans + last) % MOD
    return ans
