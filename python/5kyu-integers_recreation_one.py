"""
Divisors of 42 are: 1, 2, 3, 6, 7, 14, 21, 42. These divisors squared are: 1, 4, 9, 36, 49, 196,
441, 1764.

The sum of the squared divisors is 2500 which is 50*50, a square!

Given two integers m and n (1 < m < n) we want to find all integers between m and n whose sum
of squared divisors is itself a square, 42 is such a number.

The result will be an array of arrays or of tuples (in C an array of Pair) or a string, each
subarray having two elements, first the number whose squared divisors is a square and then the
sum of the squared divisors.

Examples:

list_squared(1, 250) --> [[1, 1], [42, 2500], [246, 84100]]

The form of the examples may change according to the language, see Example Tests for more details.

NOTE
In Fortran, as in any other language, the returned string is not permitted to contain any redundant
trailling whitespace, you can use dynamically allocated character strings.
"""

# My Answer
import math

def get_divisors(n):
    store_divisors = []
    i = 1
    while i <= math.sqrt(n):
        if (n % i == 0):
            if (n / i == i):
                store_divisors.append(i)
            else:
                store_divisors.append(n/i)
                store_divisors.append(i)
        i = i + 1
    store_divisors = [int(elem**2) for elem in store_divisors]
    return sum(sorted(store_divisors))

def is_perfect_square(x):
    sr = math.sqrt(x)
    if ((sr - math.floor(sr)) == 0):
        return int(sr)
    else:
        return "No valid number"

def list_squared(m, n):
    result = []
    for num in range(m, n):
        sum_squared_divisors = get_divisors(num)
        candidate = is_perfect_square(sum_squared_divisors)
        if isinstance(candidate, int):
            result.append([num, sum_squared_divisors])
    return result

# Best Answer
CACHE = {}

def squared_cache(number):
    if number not in CACHE:
        divisors = [x for x in range(1, number + 1) if number % x == 0]
        CACHE[number] = sum([x * x for x in divisors])
        return CACHE[number]

    return CACHE[number]

def list_squared(m, n):
    ret = []

    for number in range(m, n + 1):
        divisors_sum = squared_cache(number)
        if (divisors_sum ** 0.5).is_integer():
            ret.append([number, divisors_sum])

    return ret

# Tests Suite
Test.assert_equals(list_squared(1, 250), [[1, 1], [42, 2500], [246, 84100]])
Test.assert_equals(list_squared(42, 250), [[42, 2500], [246, 84100]])
Test.assert_equals(list_squared(250, 500), [[287, 84100]])
