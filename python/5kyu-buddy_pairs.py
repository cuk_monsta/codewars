"""
You know what divisors of a number are. The divisors of a positive integer n
are said to be proper when you consider only the divisors other than n itself.

In the following description, divisors will mean proper divisors. For example
for 100 they are 1, 2, 4, 5, 10, 20, 25 and 50.

Let s(n) be the sum of these proper divisors of n. Call buddy two positive
integers such that the sum of the proper divisors of each number is one more
than the other number:

(n, m) are pair of buddy if s(m) = n + 1 and s(n) = m + 1

For example 48 & 75 is such a pair:
  - Divisors of 48 are; 1, 2, 3, 4, 6, 8, 12, 16, 24 --> sum: 76 = 75 + 1
  - Divisors of 75 are: 1, 3, 5, 15, 25 --> sum: 49 = 48 + 1

Give two positive integers start and limit, the function buddy(start, limit)
should return the first pair (n, m) of buddy pairs such that n is between
start and limit both inclusive, m can be greater than limit and has to be
greater than n.

If there is no buddy pair satisfying the conditions, then return "Nothing"
or (for Go lang) nil
"""

# My Answer
def s_function(number):
    return sum(set(d for pair in ([i, number//i] for i in range(1, int(number**0.5)+1) if not number % i) for d in pair)) - number 

def buddy(start, limit):
    for n in range(start, limit):
        m_ = s_function(n) - 1
        sum_m_ = n + 1
        if m_ < start:
            continue
        elif s_function(m_) == sum_m_:
            return [n, m_]
    return 'Nothing'

# Best Answer
def div_sum(n):
    divs = set()
    for x in range(2, int(n**0.5)+1):
        if n % x == 0:
            divs.add(x)
            divs.add(n // x)
    return sum(divs)


def buddy(start, limit):
    for n in range(start, limit+1):
        buddy = div_sum(n)

        if buddy > n and div_sum(buddy) == n:
            return [n, buddy]

    return "Nothing"

# Tests
Test.assert_equals(buddy(10, 50), [48, 75])
Test.assert_equals(buddy(2177, 4357), "Nothing")
Test.assert_equals(buddy(57345, 90061), [62744, 75495])
Test.assert_equals(buddy(1071625, 1103735), [1081184, 1331967])
