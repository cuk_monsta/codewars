"""
WIP
In mathematics, a Diophantine equation is a polynomial equation, usually with two or more unknowns,
such that only the integer solutions are sought or studied.

In this kata we want to find all integers 'x, y' (x >= 0, y >= 0) solutions of a diophantine
equation of the form:

x**2 -4 * y**2 = n

Where the unknowns are 'x' and 'y', and 'n' is a given positive number, in decreasing order of the
positive xi.

If there is no solution return [] or "[]" or "".

Examples:
solEquaStr(90005) --> "[[45003, 22501], [9003, 4499], [981, 467], [309, 37]]"
solEquaStr(90002) --> "[]"

Hint:
x**2 - 4 * y**2 = (x - 2*y) * (x + 2*y)
"""

# My Answer
def sol_equa(n):
    limit = 500
    results = []

    for x in range(0, limit):
        for y in range(0, limit):
            n_ = (x - 2 * y)*(x + 2 * y)
            if n_ == n:
                results.append([x, y])

    return results

# Another solutions
def sol_equa(n):
    limit = 700
    results = []

    x = 0

    while x < limit:
        for y in range(0, x):
            n_ = (x - 2 * y)*(x + 2 * y)
            if n_ == n:
                results.append([x, y])
        x += 1
    return results

# And other solution
import numpy as np

def sol_equa(n):
    limit = 1100
    results = []

    x = int(np.sqrt(n)) - 1

    while x < limit:
        for y in range(0, x):
            n_ = (x - 2 * y)*(x + 2 * y)
            if n_ == n:
                results.append([x, y])
        x += 1
    return sorted(results, reverse=True

# Best Answer

