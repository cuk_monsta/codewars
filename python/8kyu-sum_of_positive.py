"""
You get an array of numbers, return the sum of all of the positives ones.

Example:

[1, -4, 7, 12] => 1 + 7 + 12 = 20

NOTE: If there is nothing to sum, the sum is default to 0.
"""

# My Answer
def positive_sum(arr):
    result = [num for num in arr if num >= 0]
    return sum(result)

# Best Answer
def positive_sum(arr):
    return sum(x for x in arr if x > 0)
