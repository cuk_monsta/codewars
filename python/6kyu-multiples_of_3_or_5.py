"""
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.

The sum of these multiples is 23.

Finish the solution so that it returns the sum of all the multiples of 3 or 5 below the number
passed in.

NOTE: If the number is a multiple of both 3 and 5, only count it once.
"""

# My Answer
def solution(number):
    numbers_range = range(0, number)

    result = [num for num in numbers_range if num % 3 == 0 or num % 5 == 0]

    return sum(result)

# Refactor my answer
def solution(number):
    return sum([num for num in range(0, number) if num % 3 == 0 or num % 5 == 0])

# Best Answer
def solution(number):
    return sum(x for x in range(number) if x % 3 == 0 or x % 5 == 0)
