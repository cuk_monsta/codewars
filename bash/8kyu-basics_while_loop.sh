#Create a simple while loop in bash that prints the numbers 1-20 to stdout.

# It should look like (stdout):

# Count: 1
# Count: 2
# ...
# Count: 20

# My Answer
#!/bin/bash

countToTwenty() {
  i=1
  while [ $i -le 20 ];
  do 
    echo "Count: "$i
    i=$((i + 1))
  done
}

countToTwenty

# Best Answer
echo "Count: "{1..20}
