# Write a BASH script which checks for a given file. If the file exists, print
# "Found *fileName*" to stdout, otherwise print "Can't find *findName*".

# If no arguments are supplied, print "Nothing to find".

# My Answer
#!/bin/bash

if [[ -f "$*" ]]; then
  echo "Found $*"
elif [[ -z "$*" ]]; then
  echo "Nothing to find"
else
  echo "Can't find $*"
fi

# Best Answer
#!/bin/bash

if [ -z $1 ] ; then
  echo "Nothing to find"
elif [ -e $1 ] ; then
  echo "Found $1"
else
  echo "Can't find $1"
fi
