# The first century spans from the year 1 up to and including the year 100,
# the second - from the year 101 up to and including the year 200, etc.

# Task: Given a year, return the century it is in.

# Input, Output Examples:
#  1705 => 18
#  1900 => 19
#  1601 => 17
#  2000 => 20

# Hope you enjoy it, awaiting for best practice codes.

# My Answer
#!/bin/bash
year=$1
centuryFromYear() {
  if [ "${year: -2}" -eq "00" ]; then
    echo $((year/100))
  else
    echo $((year/100 + 1))
  fi
}

centuryFromYear

# Best Answer
#!/bin/bash
year=$1
# your code here
echo "$year/100+($year/100)*100/$year*-1+1" | bc
