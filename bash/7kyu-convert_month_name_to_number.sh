# Convert month name to number. First parameter - month, return this number.

# Examples:
#  stdin:Dec
#  stdout:12

#  stdin:Jan
#  stdout:01

# My Answer
month="$1"
echo "$(date -d "$month 1 $year" "+%m")"

# Best Answer
date -d "${1,,} 1" +%m
