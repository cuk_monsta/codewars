# Very simple, given a number, find its opposite.

# Examples:

# -1: 1
# 5: -5

# My Answer
#!/bin/bash

echo "scale=2;$1 * -1" | bc

# Best Answer
#!/bin/bash
# your code here
echo ${1}*-1 | bc
