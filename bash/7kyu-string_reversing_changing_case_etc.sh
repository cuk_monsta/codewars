# Given 2 string parameters, show a concatenation of:

# - The reverse of the 2nd string with inverted case; e.g Fish -> HSIf
# - A separator in between both strings: @@@
# - The 1st string reversed with inverted case and then mirrored; e.g Water -> RETAwwATER

# Keep in mind that this kata was initially designed for Shell, I'm aware it may be easier
# in other languages.

# My Answer
#!/bin/bash

first_word=$(echo $1 | sed -E 's/([[:lower:]])|([[:upper:]])/\U\1\L\2/g')
first_word_rev=$(echo $1 | sed -E 's/([[:lower:]])|([[:upper:]])/\U\1\L\2/g' | rev)
second_word_rev=$(echo $2 | sed -E 's/([[:lower:]])|([[:upper:]])/\U\1\L\2/g' | rev)

echo $second_word_rev@@@$first_word_rev$first_word

# Best Answer
#!/bin/bash

FOO=$(echo ${1~~} | rev);
BAR=$(echo ${2~~} | rev);
echo ${BAR}@@@${FOO}${1~~};
