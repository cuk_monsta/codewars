# Complete the solution so that it returns a formatted string. The return value should equal
# "Value is $VALUE" where value is a 5 digit padded number.

# Example

# solution(5) // should return "Value is 00005"

# My Answer
for i in $(seq -f "%05g" $1 $1)
do
  echo "Value is $i"
done

# Best Answer
printf 'Value is %05d\n' $1
